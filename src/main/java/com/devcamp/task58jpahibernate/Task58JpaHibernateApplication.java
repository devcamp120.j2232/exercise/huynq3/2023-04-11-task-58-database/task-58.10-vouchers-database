package com.devcamp.task58jpahibernate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task58JpaHibernateApplication {

	public static void main(String[] args) {
		SpringApplication.run(Task58JpaHibernateApplication.class, args);
	}

}
