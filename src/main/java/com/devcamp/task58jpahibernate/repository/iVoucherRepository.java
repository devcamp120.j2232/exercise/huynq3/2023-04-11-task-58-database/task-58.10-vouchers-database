package com.devcamp.task58jpahibernate.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task58jpahibernate.model.CVoucher;

public interface iVoucherRepository extends JpaRepository<CVoucher, Long>{
    
}
